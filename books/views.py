from django.shortcuts import render
from books.models import Book

#ANY VARIABLE MADE HERE IS ISOLATED FROM ANY OTHER FILES

def book_list(request):
    list_of_books = Book.objects.all()
    context = {
        "books": list_of_books,
    }
    return render(request, "books/book_list.html", context)