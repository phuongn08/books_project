from django.db import models

# Create your models here.

class Book(models.Model):
    title = models.CharField(max_length=200, unique=True, null=True) #the , is the independence of arguments
    author = models.CharField(max_length=200, null=True)
    pages = models.SmallIntegerField(null=True)
    isbn = models.BigIntegerField(null=True)
    year_published = models.SmallIntegerField(null=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)

    def __str__(self):
        return self.title + " by " + self.author